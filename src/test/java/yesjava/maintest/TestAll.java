package yesjava.maintest;

import org.junit.Test;
import yesjava.dao.impl.SubPassLogDaoImpl;
import yesjava.dao.impl.SubjectDaoImpl;
import yesjava.dao.impl.UserDaoImpl;
import yesjava.entity.SubPassLog;
import yesjava.entity.Subject;
import yesjava.entity.User;
import yesjava.service.SubPassLogService;
import yesjava.service.SubjectService;
import yesjava.service.UserService;
import yesjava.service.impl.SubPassLogServiceImpl;
import yesjava.service.impl.SubjectServiceImpl;
import yesjava.service.impl.UserServiceImpl;

import java.util.Date;

public class TestAll {

    UserService userService = new UserServiceImpl(new UserDaoImpl());
    SubjectService subjectService = new SubjectServiceImpl(new SubjectDaoImpl(),new UserDaoImpl());
    SubPassLogService  subPassLogService= new SubPassLogServiceImpl(new SubPassLogDaoImpl());


    @Test
    public void test1(){
        User zhanglaoshi = userService.findById(26);
        Subject subject = new Subject().setTitle("学生管理系统").setContent("管理学生的生活、通讯信息等......");
        subjectService.addSubject(zhanglaoshi,subject);

    }


    @Test
    public void test2(){
        User zhanglaoshi = userService.findById(20);
        Subject subject = subjectService.findById(77);

        String toSay = "yyds yyds yyds !!!";
        boolean passRs= true;

        SubPassLog subPassLog = new SubPassLog().setCheckerId(zhanglaoshi.getId()).setRemarks(toSay).setSubjectId(subject.getId()).setCheckResult(passRs ? 1 : 0);
        subPassLog.setDeleted(0).setCreateTime(new Date()).setUpdateTime(new Date());
        if(passRs){
            subject.setPass(passRs ? 1 : 0);
        }

        subjectService.check(zhanglaoshi,subject);
        subPassLogService.save(subPassLog);

    }
}
