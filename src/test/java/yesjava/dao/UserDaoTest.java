package yesjava.dao;


import org.junit.Before;
import org.junit.Test;
import yesjava.dao.impl.UserDaoImpl;
import yesjava.entity.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserDaoTest {
    private UserDao userDao ;

    @Before
    public void setUp() throws Exception {
       userDao = new UserDaoImpl();
    }

    @Test
    public void insertUser() {
        for (int i = 0; i < 10; i++) {
            User user = new User().setUserName("user"+i).setPassword("pwd"+i).setCreateTime(new Date());
            userDao.insertEntity(user);
        }
        System.out.println("OK");
    }

    @Test
    public void insertUsers() {
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            User user = new User().setUserName("userAA"+i).setPassword("pwdAA"+i).setCreateTime(new Date());
            list.add(user);
        }
        userDao.insertEntityList(list);
    }

    @Test
    public void delUserById() {
        User user = userDao.selelctById(12);
        userDao.delEntityById(user);
        System.out.println("OK");

    }

    @Test
    public void delUserByIds() {
        userDao.delEntityByIds(17,18,19);
    }

    @Test
    public void selelctAll() {
        List<User> users = userDao.selectAll();
        users.forEach(System.out::println);
    }

    @Test
    public void selelct() {
        //只是其实一个条件
        User userParam = new User().setUserType(1).setUserName("A3");
        List<User> userList = userDao.selectByCondition(userParam);
        userList.forEach(System.out::println);
    }

    @Test
    public void selelctById() {
        System.out.println(userDao.selelctById(25));
    }

    @Test
    public void update() {
        User user = userDao.selelctById(33);
        user.setUserName("混蛋").setPassword("密码").setRealName("hello").setPhone("123456789").setUpdateTime(new Date());
        userDao.updateById(user);

    }
}