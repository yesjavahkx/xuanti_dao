package yesjava.dao;

import org.junit.Before;
import org.junit.Test;
import yesjava.dao.impl.SubjectDaoImpl;
import yesjava.dao.impl.UserDaoImpl;
import yesjava.entity.Subject;
import yesjava.entity.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SubjectDaoTest {
    private SubjectDao dao ;
    private UserDao userDao ;

    @Before
    public void setUp() throws Exception {
        dao = new SubjectDaoImpl();
        userDao = new UserDaoImpl();
    }

    @Test
    public void insertEntity() {
        dao.insertEntity(new Subject().setTitle("OKOKOK"));
    }

    @Test
    public void insertEntityList() {

        List<Integer> ids = userDao.selectByCondition(new User().setUserType(2)).stream().map(e -> e.getId()).collect(Collectors.toList());

        List<Subject> subjectList = new ArrayList<>();
        for (int i = 0; i <108 ; i++) {
            int index = (int)(Math.random()*ids.size());
            Subject subject = new Subject().setTitle("题目-" + i).setContent("内容" + i)
                    .setSourceType((int) (Math.random() * 2)) //   随机选择老师账号的id
                    .setNeedCheck(0) // 默认需要审核
                    .setPass(0).setCreatorId(ids.get(index)).setDeleted(0)
                    .setCreateTime(new Date()).setUpdateTime(new Date());
            subjectList.add(subject);
        }
        dao.insertEntityList(subjectList);
        System.out.println("好不容易");

    }

    @Test
    public void delEntityById() {
        dao.delEntityById(new Subject().setId(11));
    }

    @Test
    public void delEntityByIds() {
        dao.delEntityByIds(12,13,14,15);
    }

    @Test
    public void selectAll() {
        List<Subject> subjectList = dao.selectAll();
        subjectList.forEach(System.out::println);
    }

    @Test
    public void select() {
        dao.selectByCondition(new Subject().setTitle("宿舍"));
    }

    @Test
    public void selelctById() {
        System.out.println(dao.selelctById(33));
    }

    @Test
    public void updateById() {
        Subject subject = dao.selelctById(66);
        subject.setContent("疫情问题估计10年后结束,所以我要做一个XX 系统记录这段美好的历史");
        dao.updateById(subject);
    }
}