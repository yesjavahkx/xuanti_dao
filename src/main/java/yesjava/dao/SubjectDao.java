package yesjava.dao;

import yesjava.entity.Subject;

public interface SubjectDao extends BaseDao<Subject> {
}
