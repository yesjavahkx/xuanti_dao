package yesjava.dao;

import yesjava.entity.SubPassLog;
import yesjava.entity.UserSubject;

public interface UserSubjectDao extends BaseDao<UserSubject> {
}
