package yesjava.dao.impl;

import cn.hutool.db.Db;
import yesjava.dao.SubPassLogDao;
import yesjava.entity.SubPassLog;

import java.sql.SQLException;

public class SubPassLogDaoImpl implements SubPassLogDao {

    @Override
    public void insertEntity(SubPassLog entity) {
        try {
            String sql = " insert into " +
                    "sub_pass_log(subject_id,checker_id,check_result,remarks,deleted,create_time,update_time) " +
                    "values (?,?,?,?,?,?,?) ";
            Db.use().execute(sql, entity.getSubjectId(), entity.getCheckerId(), entity.getCheckResult(), entity.getRemarks(),entity.getDeleted(), entity.getCreateTime(), entity.getUpdateTime());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
