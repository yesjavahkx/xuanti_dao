package yesjava.dao.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.db.Db;
import cn.hutool.db.Entity;
import yesjava.dao.SubjectDao;
import yesjava.entity.Subject;
import yesjava.entity.User;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class SubjectDaoImpl implements SubjectDao {


    @Override
    public void insertEntity(Subject entity) {
        try {
            String sql = " insert into " +
                    "xt_subject(title,content,source_type,is_pass,final_student_id,final_teacher_id,need_check,creator_id,deleted,create_time,update_time) " +
                    "values (?,?,?,?,?,?,?,?,?,?,?) ";
            Db.use().execute(sql, entity.getTitle(), entity.getContent(), entity.getSourceType(), entity.getPass(), entity.getFinalStudentId(), entity.getFinalTeacherId(), entity.getNeedCheck(), entity.getCreatorId(),entity.getDeleted(), entity.getCreateTime(), entity.getUpdateTime());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void insertEntityList(List<Subject> entityList) {
        entityList.forEach(this::insertEntity);
    }

    @Override
    public void delEntityById(Subject entity) {
        try {
            Db.use().del(
                    Entity.create("xt_subject").set("id", entity.getId())
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delEntityByIds(Integer... id) {
        try {
            Db.use().del(
                    Entity.create("xt_subject").set("id", id)//where条件
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Subject> selectAll() {
        List<Subject> subjectList = null;
        try {
            String sql = " SELECT * " +
                    "FROM xt_subject ";
            subjectList = Db.use().query(sql).stream().map(e -> {
                Subject user = new Subject();
                BeanUtil.copyProperties(e, user, true);
                return user;
            }).collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return subjectList;
    }

    @Override
    public List<Subject> selectByCondition(Subject entityCondition) {
        System.out.println("条件查询 未实现！！");
        return null;
    }

    @Override
    public Subject selelctById(Integer id) {
        List<Subject> subjectList = null;
        try {
            String sql = " SELECT * " +
                    "FROM xt_subject where id = ? ";
            subjectList = Db.use().query(sql, id).stream().map(e -> {
                Subject user = new Subject();
                BeanUtil.copyProperties(e, user, true);
                return user;
            }).collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return (subjectList != null && subjectList.size() > 0) ? subjectList.get(0) : null;

    }

    @Override
    public void updateById(Subject entity) {
        String sql = " update xt_subject set "
                + "title= ?  , "
                + "content= ?  , "
                + "source_type= ?  , "
                + "is_pass= ?  , "
                + "final_student_id= ?  , "
                + "final_teacher_id= ?  , "
                + "need_check= ?  , "
                + "creator_id= ?  , "
                + "deleted= ?  , "
                + "create_time= ?  , "
                + "update_time = ? " +
                " where id = ? ";
        try {
            Db.use().execute(sql, entity.getTitle(), entity.getContent(), entity.getSourceType(), entity.getPass(), entity.getFinalStudentId(), entity.getFinalTeacherId(), entity.getNeedCheck(), entity.getCreatorId(),entity.getDeleted(), entity.getCreateTime(), entity.getUpdateTime(), entity.getId());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
