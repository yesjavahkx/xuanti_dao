package yesjava.dao.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.db.Db;
import cn.hutool.db.Entity;
import yesjava.dao.UserDao;
import yesjava.entity.User;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

public class UserDaoImpl implements UserDao {
    @Override
    public void insertEntity(User user) {
        try {
            String sql = " insert into xt_user(user_name,password,create_time) values (?, ?, ?) ";
            Db.use().execute(sql,user.getUserName(),user.getPassword(),user.getCreateTime());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void insertEntityList(List<User> users) {
        users.forEach(this::insertEntity);
    }

    @Override
    public void delEntityById(User user) {
        try {
            Db.use().del(
                    Entity.create("xt_user").set("id",user.getId())//where条件
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delEntityByIds(Integer... id) {
        try {
            Db.use().del(
                    Entity.create("xt_user").set("id",id)//where条件
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<User> selectAll() {
        List<User> users = null;
        try {
            String sql = " SELECT * " +
                    // " id,  user_name , `password`,real_name 'realName', phone, user_type 'userType', deleted, create_time , update_time 'updateTime' " +
                    "FROM xt_user ";
            users = Db.use().query(sql).stream().map(e -> {
                User user = new User();
                BeanUtil.copyProperties(e,user,true);
                return user;
            }).collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return users;
    }

    @Override
    public List<User> selectByCondition(User entityCondition) {
        List<User> userList = null;
        try {
            Entity where = Entity.create("xt_user");
//            模糊查询
            if(entityCondition.getUserName()!=null){
                where.set("user_name","like %"+ entityCondition.getUserName().trim()+"%");
            }
            if(entityCondition.getUserType()!=null){
                where.set("user_type", entityCondition.getUserType());
            }
            userList = Db.use().findAll(where).stream().map(e -> {
                User user = new User();
                BeanUtil.copyProperties(e,user,true);
                return user;
            }).collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return userList;
    }

    @Override
    public User selelctById(Integer id) {
        List<User> users = null;
        try {
            String sql = " SELECT * " +
                   // " id,  user_name , `password`,real_name 'realName', phone, user_type 'userType', deleted, create_time , update_time 'updateTime' " +
                    "FROM xt_user where id = ? ";
            users = Db.use().query(sql,id).stream().map(e -> {
                User user = new User();
                BeanUtil.copyProperties(e,user,true);
                return user;
            }).collect(Collectors.toList());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return (users!=null && users.size()>0)? users.get(0):null;
    }

    @Override
    public void updateById(User user) {
        String sql = " update xt_user set " +
                "  user_name = ?, " +
                "`password` = ?," +
                "real_name  = ?, " +
                "phone = ? ," +
                " user_type = ? , " +
                "deleted = ?, " +
                "create_time = ? , " +
                "update_time = ?   " +
                "where id = ? ";
        try {
            Db.use().execute(sql, user.getUserName(), user.getPassword(),user.getRealName() ,user.getPhone() ,user.getUserType() ,user.getDeleted() ,user.getCreateTime() ,user.getUpdateTime() ,user.getId() );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
