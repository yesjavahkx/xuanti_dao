package yesjava.dao;

import java.util.List;

public interface BaseDao<T> {
    default void insertEntity(T entity){ throw new RuntimeException(this.getClass().getName() +": 方法未实现");}
    default void insertEntityList(List<T> entityList){throw new RuntimeException(this.getClass().getName() +": 方法未实现");}

    default void delEntityById(T entity){throw new RuntimeException(this.getClass().getName() +": 方法未实现");}
    default void delEntityByIds(Integer... id){throw new RuntimeException(this.getClass().getName() +": 方法未实现");}

    default List<T> selectAll(){
        throw new RuntimeException(this.getClass().getName() +": 方法未实现");
    }
    default List<T> selectByCondition(T entityCondition){
        throw new RuntimeException(this.getClass().getName() +": 方法未实现");
    }
    default T selelctById(Integer id){
        throw new RuntimeException(this.getClass().getName() +": 方法未实现");
    }

    default void updateById(T entity){
        throw new RuntimeException(this.getClass().getName() +": 方法未实现");
    }
}
