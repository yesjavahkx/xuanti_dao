package yesjava.entity;

import java.io.Serializable;
import java.util.Date;

public class Subject implements Serializable {
    private Integer id;
    private String title;
    private String content;
    private Integer sourceType;
    private Integer pass;
    private Integer finalStudentId;
    private Integer finalTeacherId;
    private Integer needCheck;
    private Integer creatorId;
    private Integer deleted;
    private Date createTime;
    private Date updateTime;

    public Subject() {
    }

    public Integer getId() {
        return id;
    }

    public Subject setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Subject setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Subject setContent(String content) {
        this.content = content;
        return this;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public Subject setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public Integer getPass() {
        return pass;
    }

    public Subject setPass(Integer pass) {
        this.pass = pass;
        return this;
    }

    public Integer getFinalStudentId() {
        return finalStudentId;
    }

    public Subject setFinalStudentId(Integer finalStudentId) {
        this.finalStudentId = finalStudentId;
        return this;
    }

    public Integer getFinalTeacherId() {
        return finalTeacherId;
    }

    public Subject setFinalTeacherId(Integer finalTeacherId) {
        this.finalTeacherId = finalTeacherId;
        return this;
    }

    public Integer getNeedCheck() {
        return needCheck;
    }

    public Subject setNeedCheck(Integer needCheck) {
        this.needCheck = needCheck;
        return this;
    }

    public Integer getCreatorId() {
        return creatorId;
    }

    public Subject setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
        return this;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public Subject setDeleted(Integer deleted) {
        this.deleted = deleted;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Subject setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public Subject setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", sourceType=" + sourceType +
                ", pass=" + pass +
                ", finalStudentId=" + finalStudentId +
                ", finalTeacherId=" + finalTeacherId +
                ", needCheck=" + needCheck +
                ", creatorId=" + creatorId +
                ", deleted=" + deleted +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
