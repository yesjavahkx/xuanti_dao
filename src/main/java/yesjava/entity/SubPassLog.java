package yesjava.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * sub_pass_log
 * @author 
 */

public class SubPassLog implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    private Integer id;

    /**
     * 题目id
     */
    private Integer subjectId;

    /**
     * 审核者id
     */
    private Integer checkerId;

    /**
     * 审核的结果
     */
    private Integer checkResult;

    /**
     * 备注，审核说明
     */
    private String remarks;

    /**
     * 删除标识
     */
    private Integer deleted;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    public SubPassLog() {
    }

    public Integer getId() {
        return id;
    }

    public SubPassLog setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public SubPassLog setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
        return this;
    }

    public Integer getCheckerId() {
        return checkerId;
    }

    public SubPassLog setCheckerId(Integer checkerId) {
        this.checkerId = checkerId;
        return this;
    }

    public Integer getCheckResult() {
        return checkResult;
    }

    public SubPassLog setCheckResult(Integer checkResult) {
        this.checkResult = checkResult;
        return this;
    }

    public String getRemarks() {
        return remarks;
    }

    public SubPassLog setRemarks(String remarks) {
        this.remarks = remarks;
        return this;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public SubPassLog setDeleted(Integer deleted) {
        this.deleted = deleted;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public SubPassLog setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public SubPassLog setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return "SubPassLog{" +
                "id=" + id +
                ", subjectId=" + subjectId +
                ", checkerId=" + checkerId +
                ", checkResult=" + checkResult +
                ", remarks='" + remarks + '\'' +
                ", deleted=" + deleted +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}