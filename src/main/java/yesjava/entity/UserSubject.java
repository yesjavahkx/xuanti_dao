package yesjava.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * teacher_stu_subject
 *
 * @author
 */
public class UserSubject implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    private Integer id;

    /**
     * 学生id
     */
    private Integer studentId;

    /**
     * 老师id
     */
    private Integer teacherId;

    /**
     * 题目id
     */
    private Integer subjectId;

    /**
     * 是否确认
     */
    private Integer ok;

    /**
     * 删除标识
     */
    private Integer deleted;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    public UserSubject() {
    }

    public Integer getId() {
        return id;
    }

    public UserSubject setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public UserSubject setStudentId(Integer studentId) {
        this.studentId = studentId;
        return this;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public UserSubject setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
        return this;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public UserSubject setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
        return this;
    }

    public Integer getOk() {
        return ok;
    }

    public UserSubject setOk(Integer ok) {
        this.ok = ok;
        return this;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public UserSubject setDeleted(Integer deleted) {
        this.deleted = deleted;
        return this;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public UserSubject setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public UserSubject setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @Override
    public String toString() {
        return "UserSubject{" +
                "id=" + id +
                ", studentId=" + studentId +
                ", teacherId=" + teacherId +
                ", subjectId=" + subjectId +
                ", ok=" + ok +
                ", deleted=" + deleted +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}