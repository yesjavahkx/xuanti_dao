package yesjava.service;

import yesjava.entity.User;

public interface UserService extends BaseService<User> {
    boolean login(User user);
}
