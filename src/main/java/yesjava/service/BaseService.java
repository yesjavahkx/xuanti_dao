package yesjava.service;

import java.util.List;

public interface BaseService<T> {
   void save(T entity);
   void save(List<T> entityList);
   T findById(Integer id);
   List<T> findAll();
   void removeByIds(Integer... ids);
   void modifyById(T entity );
}
