package yesjava.service;

import yesjava.entity.Subject;
import yesjava.entity.User;

public interface SubjectService extends BaseService<Subject> {
    public void check(User teacher, Subject subject);

    //出题
    public void addSubject(User user,Subject subject);

}
