package yesjava.service.impl;

import yesjava.dao.BaseDao;
import yesjava.service.BaseService;

import java.util.List;

public class BaseServiceImpl<T> implements BaseService<T> {
    protected BaseDao<T> baseDao;

    public BaseServiceImpl(BaseDao<T> baseDao) {
        this.baseDao = baseDao;
    }

    @Override
    public void save(T entity) {
        baseDao.insertEntity(entity);
    }

    @Override
    public void save(List<T> entityList) {
        baseDao.insertEntityList(entityList);
    }


    @Override
    public T findById(Integer id) {
        return baseDao.selelctById(id);
    }

    @Override
    public List<T> findAll() {
        return baseDao.selectAll();
    }


    @Override
    public void removeByIds(Integer... ids) {
        baseDao.delEntityByIds(ids);
    }

    @Override
    public void modifyById(T entity) {
        baseDao.updateById(entity);
    }
}
