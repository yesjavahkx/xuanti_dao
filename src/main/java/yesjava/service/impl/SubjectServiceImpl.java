package yesjava.service.impl;

import yesjava.dao.SubjectDao;
import yesjava.dao.UserDao;
import yesjava.entity.Subject;
import yesjava.entity.User;
import yesjava.service.SubjectService;

import java.util.Date;

public class SubjectServiceImpl extends BaseServiceImpl<Subject> implements SubjectService {
    private UserDao userDao;
    public SubjectServiceImpl(SubjectDao subjectDao,UserDao userDao) {
        super(subjectDao);
        this.userDao = userDao;
    }

    @Override
    public void check(User teacher, Subject subject) {
        this.modifyById(subject);
    }

    @Override
    public void addSubject(User user, Subject subject) {
        subject.setCreatorId(user.getId());
        subject.setDeleted(0);
        subject.setSourceType( (user.getUserType()==2?0:1));
        subject.setPass(0);
        subject.setNeedCheck(0);
        subject.setCreateTime(new Date());
        subject.setUpdateTime(new Date());
        this.save(subject);
    }
}
