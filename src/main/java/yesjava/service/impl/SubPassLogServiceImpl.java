package yesjava.service.impl;

import yesjava.dao.SubPassLogDao;
import yesjava.dao.UserDao;
import yesjava.dao.impl.UserDaoImpl;
import yesjava.entity.SubPassLog;
import yesjava.entity.User;
import yesjava.service.SubPassLogService;
import yesjava.service.UserService;

import java.util.List;

public class SubPassLogServiceImpl extends BaseServiceImpl<SubPassLog> implements SubPassLogService {
    public SubPassLogServiceImpl(SubPassLogDao dao) {
        super(dao);
    }

}
