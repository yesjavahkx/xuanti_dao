package yesjava.service.impl;

import yesjava.dao.BaseDao;
import yesjava.dao.UserDao;
import yesjava.dao.impl.UserDaoImpl;
import yesjava.entity.User;
import yesjava.service.UserService;

import java.util.List;

public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

    public UserServiceImpl(UserDao userDao) {
        super(userDao);
    }

    public boolean login(User user) {
        boolean success = false;
        List<User> users = this.baseDao.selectAll();
        for (User each : users) {
            if(each.getUserName().equals(user.getUserName()) && each.getPassword().equals(user.getPassword())){
                success = true;
                break;
            }
        }
        return success;
    }

//    todo 临时代码，需要删除
    public static void main(String[] args) {
        UserDao dao = new UserDaoImpl();
        UserService userService = new UserServiceImpl(dao);
        System.out.println(userService.login(new User().setUserName("user9").setPassword("pwd911")));
    }
}
