package generate;

import yesjava.entity.UserSubject;

public interface TeacherStuSubjectDao {
    int deleteByPrimaryKey(Integer id);

    int insert(UserSubject record);

    int insertSelective(UserSubject record);

    UserSubject selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserSubject record);

    int updateByPrimaryKey(UserSubject record);
}